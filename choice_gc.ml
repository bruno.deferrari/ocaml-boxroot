type 'a t

external map : ('a -> 'b) -> 'a t -> 'b t = "choice_gc_map"

external return : 'a -> 'a t = "choice_gc_return"
external pair : 'a t -> 'b t -> ('a * 'b) t = "choice_gc_pair"

external bind : ('a -> 'b t) -> 'a t -> 'b t = "choice_gc_bind"

external fail : unit -> 'a t = "choice_gc_fail"
external choice : 'a t -> 'a t -> 'at = "choice_gc_choice"

external run : 'a t -> ('a -> unit) -> unit = "choice_gc_run"
